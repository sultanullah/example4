/**
 * 
 * @author Sathish Gopalakrishnan
 * 
 *         This class illustrates a simple custom exception.
 * 
 */
public class IntValueNotFoundException extends Exception {
	// The following represents the value that was not found
	// Shows that exceptions can have more information
	int val;

	// constructor to create a new exception
	/**
	 * 
	 * @param val
	 *            The value that was not found
	 */
	public IntValueNotFoundException(int val) {
		this.val = val;
	}

	/**
	 * 
	 * @return the value that was not found... presumably.
	 */
	public int getVal() {
		return val;
	}
}
