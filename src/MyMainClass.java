import java.util.Arrays;
import java.util.Scanner;
import java.io.*;

public class MyMainClass {

	/**
	 * This method takes a filename as an argument and reads the file into an
	 * array of integers. The file must contain an integer per row. The method
	 * then sorts the array and asks the user to enter a value to search for.
	 * The method then uses binary search to locate the value.
	 * 
	 * @author Sathish Gopalakrishnan
	 * 
	 * @param args
	 *            Name of a file with integers
	 * @throws IOException
	 *             if an invalid filename was provided as an argument or from
	 *             the console
	 */
	public static void main(String[] args) throws IOException {
		// notice how we have indicated that main( ) can throw an exception

		String fileName;

		// We will need a scanner for any input
		Scanner scanner = new Scanner(System.in);

		if (args.length < 1) {
			System.out.println("No input file!");

			// No file name was provided as an argument.
			// Let us ask for a filename.
			System.out.print("Enter a file name: ");

			// Read the filename from the standard input
			fileName = scanner.nextLine();
		} else {
			// The user provided the filename as an argument
			// Let us grab it from args[ ]
			fileName = args[0];
		}

		// Now that we have the filename, we need to open the file
		// and read data into an array.

		// We will use length to keep track of the number of values we read
		int length = 0;

		// All we do now is to count the number of rows
		// and this is painful and ugly.
		// Should use an ArrayList instead on an array but that
		// defeats the pedagogical value of this example.
		try {
			BufferedReader inputReader = new BufferedReader(
					new InputStreamReader(new FileInputStream(fileName)));

			String row;

			while ((row = inputReader.readLine()) != null) {
				++length;
			}

		} catch (IOException e) {
			// Oops. Likely the file was not found.
			System.out
					.println("Sorry. The file was not found or was not readable. ");

			// We cannot do much more than catch the exception at this point
			// Proceeding would be meaningless
			// Let us throw the exception again and let the system handle it!
			throw e;
		}

		// Declare the array of the correct size
		int[] array = new int[length];

		// Let us start again. And now we add values to the array.
		length = 0;

		try {
			BufferedReader inputReader = new BufferedReader(
					new InputStreamReader(new FileInputStream(fileName)));

			String row;

			while ((row = inputReader.readLine()) != null) {
				int value = Integer.parseInt(row);
				array[length] = value;
				++length;
				// System.out.println(value);
			}

		} catch (IOException e) {
			// Oops. Likely the file was not found.
			System.out
					.println("Sorry. The file was not found or was not readable.");

			// We cannot do much more than catch the exception at this point
			// Proceeding would be meaningless
			// Let us throw the exception again and let the system handle it!
			throw e;
		}

		// Let us sort this array
		Arrays.sort(array);

		// Phew. Ready to search for values.
		// Let us ask for a value to search for.
		System.out.print("Enter a value to search for: ");

		int val = Integer.parseInt(scanner.next());

		try {
			System.out.println("The value " + val + " was found at index "
					+ binarySearch(array, 0, length - 1, val));
		} catch (IntValueNotFoundException e) {

			// The value was not found and we caught an exception.
			// Let us do something here. Sometimes we could do nothing and
			// sometimes we would just throw a different exception here.

			System.out.println("Sorry. Could not find " + e.getVal()
					+ ". This was in a \"catch\" block.");

			// random humour
			if (e.getVal() == 22) {
				System.out.println("Catch 22?");
			}
		}

		// Always good form to close a Scanner object.
		scanner.close();

	}

	/**
	 * 
	 * @param a
	 *            The array to search in. This array must be sorted in
	 *            non-decreasing order and we search between indices low and
	 *            high (both inclusive). This method does not check if the array
	 *            is sorted or not.
	 * @param low
	 *            The low index for the search.
	 * @param high
	 *            The high index for the search.
	 * @param val
	 *            The value to search for.
	 * @return The index of the array location where the value was found.
	 * @throws IntValueNotFoundException
	 *             when the value 'val' is not in the array.
	 */
	public static int binarySearch(int[] a, int low, int high, int val)
			throws IntValueNotFoundException {

		// We will use binary search. It is a classic recursive algorithm for
		// searching a sorted array. It requires only O(log n) comparisons when
		// a linear search would require O(n) comparisons for an array of length
		// n.

		// if the array is too small or if high < low
		if (a.length < 1 || high < low)
			throw new IntValueNotFoundException(val);

		// find the index of the median
		int mid = (low + high) / 2;

		// compare the median to what we are looking for
		if (a[mid] == val)
			return mid;

		// did not find val yet
		if (a[mid] > val) {
			// what we are looking for is smaller than the median
			// search only in that portion
			return binarySearch(a, low, mid - 1, val);
		} else {
			// what we are looking for is greater than the median
			// search in that portion of the array
			return binarySearch(a, mid + 1, high, val);
		}
	}

}
