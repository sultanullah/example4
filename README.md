Example 4: Exceptions & Recursion 
========

and some more including `Arrays` Methods

This is an example to demonstrate several concepts in the Java context. 
* The first is the use of custom exceptions. 
* The second is the Binary Search algorithm for searching sorted arrays. This is an example of recursion that goes beyond elementary computations such as Fibonacci numbers and factorials. 
* The third  aspect to think about is the use of command line arguments - the  `String[ ] args` parameter used for `main( )`.
* Lastly, this example also uses a the sort method provided by the `Arrays` class and is a reminder to look at the Java documentation for `Arrays` carefully.

**What does this example program do?** It reads a file (the filename is provided as a command line argument, and if not the user has to enter a filename) that contains integers (one per row), stores the integers in an array, sorts the array, and then searches the array using a user-supplied value to search for. The search method uses the [binary search algorithm](http://en.wikipedia.org/wiki/Binary_search_algorithm).

You will also encounter code in `MyMainClass` that suggests that one should use an `ArrayList` for convenience. An excerpt of this code segment follows.
```java
		// All we do now is to count the number of rows
		// and this is painful and ugly.
		// Should use an ArrayList instead on an array but that
		// defeats the pedagogical value of this example.
		try {
			BufferedReader inputReader = new BufferedReader(
					new InputStreamReader(new FileInputStream(fileName)));

			String row;

			while ((row = inputReader.readLine()) != null) {
				++length;
			}

		} catch (IOException e) {
			// Oops. Likely the file was not found.
			System.out
					.println("Sorry. The file was not found or was not readable. ");

			// We cannot do much more than catch the exception at this point
			// Proceeding would be meaningless
			// Let us throw the exception again and let the system handle it!
			throw e;
		}

		// Declare the array of the correct size
		int[] array = new int[length];
```

### Command Line Arguments in Eclipse
Normally command line arguments are passed to a Java program by using the following command at the command line:
`java <JavaProgram.class> <args>`
This command tells the operating system to launch the Java Virtual Machine with the given program and the `args` are passed to the program.

For the example program, we could type
`java MyMainClass.class input1.txt`
to pass `input1.txt` as the command line argument that indicates the file to use.

With Eclipse we have to set command line arguments using the `Run > Run Configurations…` menu item. If we want to pass `input1.txt` as the command line argument then we would select the `Arguments` tab in the `Run Configurations` window and enter `input1.txt` in the `Program Arguments` box.

### Recommended Reading
* [Tutorial on Exceptions](http://docs.oracle.com/javase/tutorial/essential/exceptions/)
* [Tutorial on Command Line Arguments](http://docs.oracle.com/javase/tutorial/essential/environment/cmdLineArgs.html)
* [Tutorial on Arrays](http://docs.oracle.com/javase/tutorial/java/nutsandbolts/arrays.html)
* [Documentation on the `Arrays` Class](http://docs.oracle.com/javase/7/docs/api/java/util/Arrays.html)
* [Documentation on `ArrayList`](http://docs.oracle.com/javase/7/docs/api/java/util/ArrayList.html)
